import React from 'react';
import { render } from 'react-dom';

import App from './components/App';

const root = document.createElement('div');
root.className = 'root';
document.body.appendChild(root);

render(React.createElement(App), root);
